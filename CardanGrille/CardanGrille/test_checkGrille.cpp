#include <cfixcc.h>
#include "checkGrille.h"

class test_checkGrille : public cfixcc::TestFixture
{
private:

public:
	//���� 1. ����� �������� � �������.
	void manyTheSlots()
	{
		char grille[SIZE][SIZE+1]= {"0100",
							"1010",
							"1010",
							"0001"};
		CFIX_ASSERT_MESSAGE(false==checkGrille(grille), L"Fail! The result is not the same!");
	}
	
	//���� 2. ���� �������� � �������.
	void fewTheSlots()
	{
		char grille[SIZE][SIZE+1]= {"0100",
							"0010",
							"0010",
							"0000"};
		CFIX_ASSERT_MESSAGE(false==checkGrille(grille), L"Fail! The result is not the same!");
	}

	//���� 3. ��������� � 3 � 4 ��������.
	void overlay3And4Quarters()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							 "0100",
							 "0000",
							 "1010"};
		CFIX_ASSERT_MESSAGE(true==checkGrille(grille), L"Fail! The result is not the same!");
	}

	//���� 4. ���������� �������.
	void correctGrille()
	{
		char grille[SIZE][SIZE+1] = {"0100",
							 "1000",
							 "0010",
							 "0001"};

		CFIX_ASSERT_MESSAGE(true==checkGrille(grille), L"Fail! The result is not the same!");
	}

	//���� 5. ����������� �������.
	void incorrectGrille()
	{
		char grille[SIZE][SIZE+1] = {"0110",
							 "0000",
							 "0000",
							 "0110"};

		CFIX_ASSERT_MESSAGE(false==checkGrille(grille), L"Fail! The result is not the same!");
	}

	//���� 6. �������� �������
	void invalidCharacters()
	{
		char grille[SIZE][SIZE+1] = {"0100",
							 "100a",
							 "9010",
							 "0001"};
		CFIX_ASSERT_MESSAGE(false==checkGrille(grille), L"Fail! The result is not the same!");
	}

};

CFIXCC_BEGIN_CLASS(test_checkGrille)
	CFIXCC_METHOD(manyTheSlots)
	CFIXCC_METHOD(fewTheSlots)
	CFIXCC_METHOD(overlay3And4Quarters)
	CFIXCC_METHOD(correctGrille)
	CFIXCC_METHOD(incorrectGrille)
	CFIXCC_METHOD(invalidCharacters)
CFIXCC_END_CLASS()

