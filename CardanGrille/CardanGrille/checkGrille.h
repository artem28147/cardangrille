#pragma once
#include "grilleSize.h"
#include "turnGrille.h"
/*!
 *\brief ������� �������� ������������ ������� ������� �������.
 *\param[in] grille ������ �������.
 *\return ��������� �� ������
 */
bool checkGrille (char grille[SIZE][SIZE+1]);

/*!
 *\brief ������� �������� ���������� �������� � �������.
 *\param[in] grille ������ �������.
 *\return �������� �� ���������� ��������
 */
bool grilleHasCorrectNumberOfSlots(char grille[SIZE][SIZE+1]);

/*!
 *\brief ������� ����������� ��� ������� ������� ������ �� 0 � 1.
 *\param[in] grille ������ �������.
 *\return �������� �� ������ �������.
 */
bool grilleConsistsOnlyOfOnesAndZeros(char grille[SIZE][SIZE+1]);

/*!
 *\brief ������� ����������� ��� ������� � ������� �� �������������.
 *\param[in] grille ������ �������.
 *\return ����������� �� �����. ���� �� ����������� true, ���� ����������� false.
 */
bool grilleSlotsAreNotRepeated(char grille[SIZE][SIZE+1]);