#include "commandLineReading.h"

error commandLineReading(int argc, char* argv[], char *inputFilename, char* outputFilename, char* grilleFilename, char* mode)
{
	error err=noError; //������

	//���� ���������� ���������� ��������� ������ ������ 5
	if(argc>5)
		//������� ������ � ������� ���������� ��������� ������
		err=excessCountOfArguments;
	//���� ���������� ���������� ��������� ������ ����� 1
	else if(argc==1)
		//������� ������, ��������� � ��� ��� ��� ������
		err=noMode;
	//���� ����� ������ �� ����� 2 � � ������ ���������� �� "-e" ��� "-d"
	else if(strlen(argv[1])!=2 && (strcmp(argv[1], "-e")!=0 || strcmp(argv[1], "-d")!=0))
		//������� ������, ��������� � �������� ������
		err=incorrectMode;
	//���� ���������� ���������� ����� 2
	else if(argc==2)
		//������� ������, ��������� � ��� ��� ��� ����� �������� ����� � �������
		err=noTextFileName;
	//���� ���������� ���������� ��������� ������ ����� 3
	else if(argc==3)
		//������� ������, ��������� � ���, ��� ��� ����� ����� � ��������
		err=noGrilleFilename;
	//���� ���������� ���������� ��������� ������ ����� 4
	else if(argc==4)
		//������� ������, ��������� � ���, ��� ��� ����� ��������� �����
		err=noOutFilename;
	//���� ������ ���
	else if(err==noError)
	{
		//�������� ����� �� ��������� ������
		strcpy(mode,argv[1]);
		//�������� ��� �������� ����� �� ��������� ������
		strcpy(inputFilename, argv[2]);
		//�������� ��� ����� ����������� ������� �� ��������� ������
		strcpy(grilleFilename, argv[3]);
		//�������� �� ��������� ������ ��� ��������� �����
		strcpy(outputFilename, argv[4]);
	}
	//������� ������
	return err;
}