#include "checkGrille.h"

bool checkGrille (char grille[SIZE][SIZE+1])
{
	//���� ������� ����� ������ ���������� ��������
	if(grilleHasCorrectNumberOfSlots(grille))
	{
		//���� ������� ������� ������ �� 0 � 1
		if(grilleConsistsOnlyOfOnesAndZeros(grille))
		{
			//���� ������� �� ����������� ��� ���������
			if(grilleSlotsAreNotRepeated(grille))
			{
				return true;
			}
		}
	}

	return false;
}

bool grilleHasCorrectNumberOfSlots(char grille[SIZE][SIZE+1])
{
	int countSlots=0; //���������� ��������

	//��������� ����������� �������� � �������
	for(int i=0; i<SIZE; i++)
	{
		for(int j=0; j<SIZE+1; j++)
		{
			if(grille[i][j]=='1')
			{
				countSlots++;
			}
		}
	}
	
	//���� ���������� �������� � ������� �� ����� 25 ��� 4 (��� ������)
	if(countSlots!=25)
	{
		if(countSlots!=4)
		{
			//������� false
			return false;
		}
	}
	return true;
}

bool grilleConsistsOnlyOfOnesAndZeros(char grille[SIZE][SIZE+1])
{
	//���������, ��� ������� ������� ������ �� 0 � 1.
	for(int i=0; i<SIZE; i++)
	{
		for(int j=0; j<SIZE; j++)
		{
			if(grille[i][j]!='0' && grille[i][j]!='1')
			{
				return false;
			}
		}
	}

	return true;
}

bool grilleSlotsAreNotRepeated(char grille[SIZE][SIZE+1])
{
	char bufGrille[SIZE][SIZE+1]; //������� ������

	//��� ������ ������
	for(int i=0; i<SIZE; i++)
	{
		//��� ������� �������
		for(int j=0; j<SIZE+1; j++)
		{

			//���������� �������� �������
			bufGrille[i][j]=grille[i][j];
		}
	}
	//��������� 3 ����
	for(int k=0; k<3; k++)
	{
		//��������� ������� �� 90 �������� � ������� ������� turnGrille
		turnGrille(bufGrille);
		
		//���� � �������� � ���������� ������� ���������� �������
		for(int m=0; m<SIZE; m++)
		{
			for(int n=0; n<SIZE+1; n++)
			{
				if(bufGrille[m][n]=='1' && grille[m][n]=='1')
				{
					//������� false
					return false;
				}
			}
		}
	}
	return true;
}