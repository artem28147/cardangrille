#include "commandLineReading.h"
#include "encryptDecrypt.h"
#include "checkGrille.h"
#include "turnGrille.h"
#include "readWriteFunctions.h"
#include "logFunction.h"
#include <clocale>
int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "Russian");
	char inputFilename[256]="";					//��� ����� �� ������� �������
	char outputFilename[256]="";				//��� ��������� �����
	char grilleFilename[256]="";				//��� ����� � ��������
	char logFilename[256]="log_";					//��� ����� ����
	char mode[3]="";							//����� ������ ���������
	char grille[SIZE][SIZE+1]={""};			//�������
	error err=noError;						//������
	bool grilleChecking=false;				//��������� �� �������
	bool endOfText=false;					//����� ��� ���������� ����������
	char inputString[SIZE*SIZE+1]="";		//������� ������
	char bufString[SIZE*SIZE+1]="";			//������, ���������� 100 �������������� ��������
	char outputString[10001]="";			//�������� ������
	int encryptDecryptCount=0;				//������� ��� ����������� ��� ������������� 100 �������� � ������� ��������� ������ � ����
	//��������� ��������� ��������� ������
	err=commandLineReading(argc, argv, inputFilename, outputFilename, grilleFilename, mode);
	if (strlen(inputFilename)==0)
	{
		strcat (logFilename, "noTextFilename.txt");
	}
	else
	{
		strcat (logFilename, inputFilename);
	}
	//���� ���� ������
	if (err!=noError)
	{	
		char errorInfo[350]="error: ";
		switch (err)
		{
			case noMode:
				strcpy (errorInfo, "�� ������ ����� ������ ���������.");
				break;
			case incorrectMode:
				strcpy (errorInfo, "����� ������ ������ �����������.");	
				break;
			case noTextFileName:
				strcpy (errorInfo, "�� ������� ��� �������� ����� � �������.");	
				break;
			case noGrilleFilename:
				strcpy (errorInfo, "�� ������� ��� �������� ����� � ��������.");	
				break;
			case noOutFilename:
				strcpy (errorInfo, "�� ������� ��� ��������� �����.");	
				break;
			case excessCountOfArguments:
				strcpy (errorInfo, "������� ���������� ���������� ����������.");	
				break;
		}
		//������� ������ � �������
		perror(errorInfo);
		//������� ������ � ���
		writeLog(logFilename, errorInfo);
	}
	else
	{
		writeLog(logFilename, "info: ������ ��������� ��������.");
		//������ �������
		err=readGrilleFromFile(grilleFilename,grille);
		if (err==readFileError)
		{
			writeLog(logFilename, "error: ������ ��� �������� ����� � �������� ��� ������.");
		}
		else
		{
			grilleChecking=checkGrille(grille);
			//���� ������� �����������
			if (!grilleChecking)
			{
				//��������� ������
				err=incorrectGrilleError;
				//������� ������ � �������
				perror("error: ������� ����� �������� ������.");
				//������� ������ � ���
				writeLog(logFilename, "error: ������� ����� �������� ������.");
			}
			else
			//���� ������ ���
			{
				if (strcmp(mode,"-e")==0)
				{
					writeLog(logFilename, "info: ������ ����� ����������.");
				}
				else if (strcmp(mode,"-d")==0)
				{
					writeLog(logFilename, "info: ������ ����� ������������.");
				}
				//���� �� ��������� ��� ������� �������� ������
				while (!endOfText && err==noError)
				{
					//������ 100 ��������� �������� �������� ������
					err=readTextBlockFromFile(inputFilename, logFilename, inputString, &endOfText);
					//���� ������ ��� � ����� �� ����������
					if (err==noError)
					{
						//�������� ��������/����������
						encryptDecrypt(grille, inputString, bufString, mode);
						//���� ���� ��� ���������
						if (strlen(bufString))
						{
							//�������� 100 ��������������/������������� �������� � �������� ������
							strcat(outputString, bufString);
						}
						//������� �����
						bufString[0]=0;
						encryptDecryptCount++;
					}
					//���������� ����� � ���� � �������� ������
					if (err==noError && (endOfText || encryptDecryptCount==100))
					{
						err=writeFile(outputFilename, outputString);
						//������� �������� ������
						outputString[0]=0;
						encryptDecryptCount=0;
						if (err==writeFileError)
						{
							//������� ������ � ���
							writeLog(logFilename,"error: ������ ��� �������� ����� � ������� ��� ������.");
						}
					}
				}
				if (err==noError)
				{
					if (strcmp(mode,"-e")==0)
					{
						writeLog(logFilename, "info: ����� ����������.");
					}
					else if (strcmp(mode,"-d")==0)
					{
						writeLog(logFilename, "info: ����� ������������.");
					}
				}
			}
		}
	}
	return 0;
}
