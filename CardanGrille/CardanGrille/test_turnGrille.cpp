#include <cfixcc.h>
#include "turnGrille.h"
class test_turnGrille : public cfixcc::TestFixture
{
private:

public:
	void voidsInOneQuarter_first()
	{
		char grille[SIZE][SIZE+1] = {	"1100",
								"1100",
								"0000",
								"0000"};
		char expectedGrille[SIZE][SIZE+1] = {	"0011",
										"0011",
										"0000",
										"0000"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInOneQuarter_second()
	{
		char grille[SIZE][SIZE+1] = {	"0011",
								"0011",
								"0000",
								"0000"};
		char expectedGrille[SIZE][SIZE+1] = {	"0000",
										"0000",
										"0011",
										"0011"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInOneQuarter_third()
	{
		char grille[SIZE][SIZE+1] = {	"0000",
								"0000",
								"0011",
								"0011"};
		char expectedGrille[SIZE][SIZE+1] = {	"0000",
										"0000",
										"1100",
										"1100"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInOneQuarter_fourth()
	{
		char grille[SIZE][SIZE+1] = {	"0000",
								"0000",
								"1100",
								"1100"};
		char expectedGrille[SIZE][SIZE+1] = {	"1100",
										"1100",
										"0000",
										"0000"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInTwoQuarters_first()
	{
		char grille[SIZE][SIZE+1] = {	"1100",
								"0000",
								"0011",
								"0000"};
		char expectedGrille[SIZE][SIZE+1] = {	"0001",
										"0001",
										"0100",
										"0100"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInTwoQuarters_second()
	{
		char grille[SIZE][SIZE+1] = {	"0001",
								"0001",
								"0100",
								"0100"};
		char expectedGrille[SIZE][SIZE+1] = {	"0000",
										"1100",
										"0000",
										"0011"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInTwoQuarters_third()
	{
		char grille[SIZE][SIZE+1] = {	"0000",
								"1100",
								"0000",
								"0011"};
		char expectedGrille[SIZE][SIZE+1] = {	"0010",
										"0010",
										"1000",
										"1000"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInTwoQuarters_fourth()
	{
		char grille[SIZE][SIZE+1] = {	"0010",
								"0010",
								"1000",
								"1000"};
		char expectedGrille[SIZE][SIZE+1] = {	"1100",
										"0000",
										"0011",
										"0000"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInAllQuarters_first()
	{
		char grille[SIZE][SIZE+1] = {	"0010",
								"0100",
								"0000",
								"1010"};
		char expectedGrille[SIZE][SIZE+1] = {	"1000",
										"0010",
										"1001",
										"0000"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInAllQuarters_second()
	{
		char grille[SIZE][SIZE+1] = {	"1000",
								"0010",
								"1001",
								"0000"};
		char expectedGrille[SIZE][SIZE+1] = {	"0101",
										"0000",
										"0010",
										"0100"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInAllQuarters_third()
	{
		char grille[SIZE][SIZE+1] = {	"0101",
								"0000",
								"0010",
								"0100"};
		char expectedGrille[SIZE][SIZE+1] = {	"0000",
										"1001",
										"0100",
										"0001"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInAllQuarters_fourth()
	{
		char grille[SIZE][SIZE+1] = {	"0000",
								"1001",
								"0100",
								"0001"};
		char expectedGrille[SIZE][SIZE+1] = {	"0010",
										"0100",
										"0000",
										"1010"};
		turnGrille(grille);
		bool equal = true;
		for (int i=0; i<4; i++)
		{
			if (strcmp(grille[i],expectedGrille[i]))
			{
				equal=false;
			}
		}
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}
};

CFIXCC_BEGIN_CLASS(test_turnGrille)
	CFIXCC_METHOD(voidsInOneQuarter_first)
	CFIXCC_METHOD(voidsInOneQuarter_second)
	CFIXCC_METHOD(voidsInOneQuarter_third)
	CFIXCC_METHOD(voidsInOneQuarter_fourth)
	CFIXCC_METHOD(voidsInTwoQuarters_first)
	CFIXCC_METHOD(voidsInTwoQuarters_second)
	CFIXCC_METHOD(voidsInTwoQuarters_third)
	CFIXCC_METHOD(voidsInTwoQuarters_fourth)
	CFIXCC_METHOD(voidsInAllQuarters_first)
	CFIXCC_METHOD(voidsInAllQuarters_second)
	CFIXCC_METHOD(voidsInAllQuarters_third)
	CFIXCC_METHOD(voidsInAllQuarters_fourth)
CFIXCC_END_CLASS()