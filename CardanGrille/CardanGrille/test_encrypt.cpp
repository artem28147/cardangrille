#include <cfixcc.h>
#include "encryptDecrypt.h"
class test_encrypt : public cfixcc::TestFixture
{
private:

public:
	void voidsInOneQuarter()
	{
		char grille[SIZE][SIZE+1] = {"1100",
							  "1100",
							  "0000",
							  "0000"};
		
		char string[15]="������, �����!";
		char encryptedString[17]="                ";
		char expectedResult[17] = "������, �!��  ��";
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");

	}

	void voidsInTwoQuarters()
	{
		char grille[SIZE][SIZE+1] = { "0100",
							  "1000",
							  "0010",
							  "0001"};
		
		char string[15]="������, �����!";
		char encryptedString[17]="";
		char expectedResult[17] = "������!� ,��  ��";
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void voidsInAllQuarters()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char string[15]="������, �����!";
		char encryptedString[17]="";
		char expectedResult[17] = "�������!, � ��� ";
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void shortText()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char string[5]="Text";
		char encryptedString[17]="";
		char expectedResult[17] = "  T  e      x t ";
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void oneSymbol()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char string[2]="a";
		char encryptedString[17]="";
		char expectedResult[17] = "  a             ";
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void space()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char string[2]=" ";
		char encryptedString[17]="";
		char expectedResult[17] = "                ";
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	void emptyString()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char string[1]="";
		char encryptedString[17]="";
		char expectedResult[17] = "                "; 
		encryptDecrypt(grille, string, encryptedString,"-e");
		bool equal = !strcmp(encryptedString,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}
};

CFIXCC_BEGIN_CLASS(test_encrypt)
	CFIXCC_METHOD(voidsInOneQuarter)
	CFIXCC_METHOD(voidsInTwoQuarters)
	CFIXCC_METHOD(voidsInAllQuarters)
	CFIXCC_METHOD(shortText)
	CFIXCC_METHOD(oneSymbol)
	CFIXCC_METHOD(space)
	CFIXCC_METHOD(emptyString)
CFIXCC_END_CLASS()