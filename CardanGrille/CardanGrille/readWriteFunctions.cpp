#include "error.h"
#include "stdio.h"
#include <string>
#include "grilleSize.h"
#include "logFunction.h"
error readTextBlockFromFile (char* textFilename, char* logFilename, char string[101], bool* endOfText)
{
	error err=noError;					//������
	static FILE *file;							//���� ��� ��������
	static char buf[10001];				//����� �� 10001 ������
	static int returnsCount=0;			//���������� ������ �� 100 ��������, ������� ��� ������� �������
	static int wholeBlocksCount=0;		//���������� ����� ������ �� 100 ��������, ������� ���������� �������
	static int partialBlockLength=0;	//����� ���������� �����, ������� ����� ���� ����� 100 �������� (0 - ���� ������ ����� ���)
	static long fileSize=0;				//������ �������� �����
	//������� ���� � ������� �� ������
	if(!fileSize)
	{
		file = fopen(textFilename, "rb");
		//���� ���� ��������
		if (file)
		{
			fseek (file, 0, SEEK_END);
			//������ ����� �����
			fileSize = ftell(file);
			fseek (file, 0, SEEK_SET);
			//������ ���������� ������ �� 100 ��������
			wholeBlocksCount = fileSize/100;
			//������ ����� ���������� �����
			partialBlockLength = fileSize%100;
			if (!fileSize)
			{
				*endOfText=true;
			}
		}
	}
	//���� ���� �� �������� ���������
	if (!file)
	{
		char errorInfo[350] = "error: ������ ��� �������� ����� � ������� ��� ������.";	//������ � ������� ��� ������ � ������ � ���
		//�������� ��� ������ �������� �� ����
		err=readFileError;
		//������� ������ � ����� ������ ������
		perror(errorInfo);
		//������� ������ � ���
		writeLog(logFilename, errorInfo);
	}
	else
	{
		strcpy(string,"");;
		//���� ������ ������������ ������ ��� ��� ���� ���������� ��������� �����
		if (returnsCount%100==0)
		{
			//writeLog(textFilename, "debug: �������� <����� �����> ���� �������� �� �����.);
			//������� � ����� 10000 ����
			fread(buf,1,10000,file);
			returnsCount=0;
		}
		//������� 100 ��� ����� ��������� �������� ������ � ������
		//���� ������� ������ ��������� ����
		if (wholeBlocksCount==0 && partialBlockLength!=0)
		{
			//������� � ������ ��������� ���� 
			strncpy(string, buf+100*returnsCount, partialBlockLength);
			string[partialBlockLength]=0;
			returnsCount++;
			//������������������ � ���, ��� ���� ����� ���������
			*endOfText=true;
		}
		//�����
		else
		{
			//������� ���� �� 100 ��������
			strncpy(string, buf+100*returnsCount, 100);
			returnsCount++;
			//��������� ���������� ������ �� 100 ��������
			wholeBlocksCount--;
		}
		//���� ������ �� ��������
		if (wholeBlocksCount==0 && partialBlockLength==0)
		{
			//������������������ � ���, ��� ���� ����� ���������
			*endOfText=true;
			fclose(file);
		}
	}
	//������� ��� ������ (0 ���� ������ ��������� ��� ������). 
	return err;
}

error readGrilleFromFile (char* grilleFilename, char grille[SIZE][SIZE+1])
{
	error err=noError;	//������
	FILE *file; //���� ��� ������
	int i=0;	//������� �����
	//������� ���� � �������� �� ������
	file = fopen(grilleFilename,"r");
	//���� ���� �� �������� ���������
	if(file == NULL)
	{
		char errorInfo[350] = "error: ������ ��� �������� ����� c �������� ��� ������.";	//������ � ������� ��� ������ � ������ � ���
		//�������� ��� ������ �������� �� ����
		err=writeFileError;
		//������� ������ � ����� ������ ������
		perror(errorInfo);
	}
	//����� 
	else
	{
		//���� �� ��������� 10 ����� � �� ��������� ����� �����
		while (i<10 && !feof(file))
			//������ ������ 10 �������� ������ � ������ ����� �������
			{
             fscanf(file,"%10s",grille[i]);
			 i++;
			}
		//������� ����
		fclose(file);
	}
	//������� ��� ������ (0 ���� ������ ��������� ��� ������)
	return err;
}

error writeFile (char* textFilename, char* string)
{
	error err=noError; //������
	int size; //���������� ������
	static FILE *file; //����
	static bool fileIsOpened=false;
	//������� �������� ���� � ������� ��� ��� ������
	if(!fileIsOpened)
	{
		file=fopen(textFilename, "ab");
		fileIsOpened=true;
	}
	//���� ���� �� �������� ���������
	if (!file)
	{
		char errorInfo[350] = "error: ������ ��� �������� ����� ��� ������ ������.";	//������ � ������� ��� ������ � ������ � ���
		//�������� ��� ������ �������� �� ����
		err=writeFileError;
		//������� ������ � ����� ������ ������
		perror(errorInfo);
	}
	//�����
	else
	{
		//�������� 10000 ���� �� ������ � �������� ����
		size=fwrite(string,1,strlen(string),file);
		fflush(file);
	}
	//������� ��� ������ (0 ���� ������ ��������� ��� ������)
	if(size==10000)
	{
		return err;
	}
}