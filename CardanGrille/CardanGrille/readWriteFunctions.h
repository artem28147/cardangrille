#pragma once
#include "error.h"
#include "grilleSize.h"
/*!
 *\brief ������� ������ ��������� 100 �������� ������ �� �����.
 *\param[in] textFilename ��� �������� ����� � �������.
 *\param[out]endOfText ����� �������� �� �����
 *\param[out] string ������ � ��������� ��������� ������.
 *\return ��� ������.
 */
error readTextBlockFromFile (char* textFilename, char* logFilename, char string[101], bool* endOfText);

/*!
 *\brief ������� ������ ������� ������ �� �����.
 *\param[in] grilleFilename ��� �������� ����� � ��������.
 *\param[out] grille ������ ������� �������.
 *\return ��� ������.
 */
error readGrilleFromFile (char* grilleFilename, char grille[SIZE][SIZE+1]);

/*!
 *\brief ������� ������ �������� ������.
 *\param[in] textFilename ��� ��������� ����� � �������.
 *\param[in] string ������ � ������� ��� ������.
 *\return ��� ������.
 */
error writeFile (char* textFilename, char* string);


