#include "encryptDecrypt.h"
void encryptDecrypt (char grille[SIZE][SIZE+1], char inputString[SIZE*SIZE+1], char outputString[SIZE*SIZE+1], char* mode)
{
	int	symbolsCount=0;			//���������� �������� ���������� � �������� �� ��������
	int totalSymbolsCount=0;	//����� ���������� ���������� ��������
	//��������� 4 ����
	for (int i=0; i<4; i++)
	{
		//���� �� �������� ��� ������� ������� �� ��������
		while (symbolsCount < (SIZE*SIZE/4))
		{
			//��� ���� ����� �������
			for (int j=0; j<SIZE; j++)
			{
				//��� ���� �������� �������
				for (int k=0; k<SIZE; k++)		
				{
					//���� � ������� �� ������ ������� ���� ���������
					if(grille[j][k]=='1')		
					{
						//���� ������ ����� ��������
						if (strcmp(mode, "-e")==0)
						{
							//�������� ������ � ������������� ������ �� ������� ��������� � �������� ������� ���������� ��������
							if (strlen(inputString)>(SIZE*SIZE/4)*i + symbolsCount)
							{
								outputString[j*SIZE+k]=inputString[(SIZE*SIZE/4)*i + symbolsCount];	
							}
							else
							{
								outputString[j*SIZE+k]=' ';	
							}
						}
						//����� ���� ������ ����� ����������
						else if (strcmp(mode, "-d")==0)
						{
							//�������� ������ ������� �� ������� ���������
							outputString[totalSymbolsCount]=inputString[j*SIZE+k];
							totalSymbolsCount++;
						}
						symbolsCount++;
					}
				}
			}
		}
		//���� ���������� �� ���������
		if (i!=3)
		{
			//��������� ������� �� 90 �������� �� ������� �������
			turnGrille(grille);
			symbolsCount=0;
		}
	}
}