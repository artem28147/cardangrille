#include "logFunction.h"


bool writeLog(char* filename, char* string)
{
	char outputString[350]="";
	FILE *logFile; //����
	bool logFileIsOpened=false;
	//�������� ������� ���� � �����
	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);
	//�������� ���� � ����� � ������
	strcat(outputString, asctime(timeinfo));
	strcat(outputString, " - ");
	//�������� � ������ � ����� � �������� ������� ������
	strcat(outputString, string);
	//�������� ������� ������
	strcat(outputString, "\n");
	
	if(!logFileIsOpened)
	{
		//������� ���� ��� ������
		logFile=fopen(filename, "a");
		logFileIsOpened=true;
	}
	//���� ���� ��� ������� ������
	if(logFile)
	{
		//�������� � ���� �������� ������
		fwrite(outputString,1,strlen(outputString),logFile);
		//������� true
		return true;
	}
	//�����
	else
		//������� false
		return false;
}