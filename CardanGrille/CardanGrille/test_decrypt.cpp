#include <cfixcc.h>
#include "encryptDecrypt.h"
#include <string.h>
#include "error.h"

class test_decrypt : public cfixcc::TestFixture
{
private:

public:
	//1. � ������� ������� ��������� � ������� ����� ��������	
	void voidsInOneQuarter()
	{
		char grille[SIZE][SIZE+1] = {"1100",
							  "1100",
							  "0000",
							  "0000"};
		char encryptedString[17] = "������, �!��  ��";
		char expectedResult[17]= "������, �����!  ";
		char string[17]="               ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
		
	}

	//2. � ������� ������� ��������� � ���� ���������
	void voidsInTwoQuarters()
	{
		char grille[SIZE][SIZE+1] = {"0100",
							  "1000",
							  "0010",
							  "0001"};
		
		char encryptedString[17] = "������!� ,��  ��";
		char string[17]="               ";
		char expectedResult[17] = "������, �����!  ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	//3. � ������� ������� ��������� �� ���� ���������
	void voidsInAllQuarters()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char encryptedString[17] = "�������!, � ���";
		char string[16]="               ";
		char expectedResult[16] = "������, �����! ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	//4. ��������� �������� �����
	void shortText()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char encryptedString[17]="  T  e      x t ";
		char string[17]="               ";
		char expectedResult[17] = "Text            ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	//5. ��������� ���� ������
	void oneSymbol()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char encryptedString[17]="  a             ";
		char string[17]="               ";
		char expectedResult[17] = "a               ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	//6. ��������� ������
	void space()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char encryptedString[17]="                ";
		char string[17]="               ";
		char expectedResult[17] = "                ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	//7. ��������� ������ ������
	void emptyString()
	{
		char grille[SIZE][SIZE+1] = {"0010",
							  "0100",
							  "0000",
							  "1010"};
		
		char encryptedString[17]="                ";
		char string[17]="                ";
		char expectedResult[17] = "                ";
		encryptDecrypt(grille, encryptedString, string, "-d");
		bool equal = !strcmp(string,expectedResult);
		CFIX_ASSERT_MESSAGE(equal, L"Fail! The result is not the same!");
	}

	

};

CFIXCC_BEGIN_CLASS(test_decrypt)
	CFIXCC_METHOD(voidsInOneQuarter)
	CFIXCC_METHOD(voidsInTwoQuarters)
	CFIXCC_METHOD(voidsInAllQuarters)
	CFIXCC_METHOD(shortText)
	CFIXCC_METHOD(oneSymbol)
	CFIXCC_METHOD(space)
	CFIXCC_METHOD(emptyString)
CFIXCC_END_CLASS()

