#include "error.h"
#include "grilleSize.h"
#include "turnGrille.h"
#include "string.h"
/*!
 *\brief ������� �������� ��� ����������.
 *\param[in] grille ������ ������� �������.
 *\param[in] string ������.
 *\param[out] encryptedString ������������� ������.
 *\param[in] mode ��������� �����.
 */
void encryptDecrypt (char grille[SIZE][SIZE+1], char string[SIZE*SIZE+1], char encryptedString[SIZE*SIZE+1], char* mode);